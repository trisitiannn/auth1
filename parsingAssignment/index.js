const axios = require('axios');
const htmlparser2 = require("htmlparser2");

var cityName = "";
var dateTime = "";
var temperature = "";

var flagForName = false;
var flagForTemp = false;
var flagForDateTime = false;
var flagForProperDt = false;
var checkText = false;

const weatherParser = new htmlparser2.Parser({
    onopentag(name, attribs) {
        if (name == "h1" && attribs.property == "name") {
            flagForName = true;
        }
        if (name == "p" && attribs.class == "text-center mrgn-tp-md mrgn-bttm-sm lead hidden-xs") {
            flagForTemp = true;
        }

        if (name == "dt") {
            checkText = true;
        }

        if (name == "dd" && attribs.class == "mrgn-bttm-0" && flagForProperDt) {
            flagForDateTime = true;
        }

    },
    ontext(text) {
        if (flagForName) {
            cityName = text;
            flagForName = false;
        }
        if (flagForTemp) {
            temperature = text;
            flagForTemp = false;
        }
        if (checkText) {
            if (text == "Date: ") {
                flagForProperDt = true;
            }
            checkText = false;
        }
        if (flagForDateTime) {
            dateTime = text;
            flagForDateTime = false;
            flagForProperDt = false;

        }
    },
    onclosetag(name) {
    }
}, { decodeEntities: true });







axios.get('https://weather.gc.ca/city/pages/bc-74_metric_e.html') // change your webpage here
    .then(function(response) {
        weatherParser.write(response.data);
        weatherParser.end();
        console.log(`${cityName}${temperature}C, ${dateTime}`);
    });
