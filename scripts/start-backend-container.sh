#!/bin/sh
# THIS SCRIPT IS INTENDED TO BE RUN FROM THE auth1 PROJECT DIRECTORY
docker run --rm -it --network my-net --name backend \
    -v $PWD/backend:/mnt/backend \
    -w /mnt/backend     bkoehler/feathersjs sh
